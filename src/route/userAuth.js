const express = require("express");
const { signup, signin } = require("../controller/userAuth");
const { validateSignUpReq, isReqValidated, validateSignInReq } = require("../validators/auth");
const router = express.Router();

router.post("/signup",validateSignUpReq, isReqValidated, signup).post("/signin", validateSignInReq, isReqValidated, signin);
// router.post('/signin', signin);

module.exports = router;

