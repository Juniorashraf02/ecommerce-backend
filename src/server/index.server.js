const express = require('express');
const app = express();
const cors = require('cors');
const dotenv = require('dotenv').config();
const colors = require('colors');
const { databaseConnect } = require('../utils/dbconnect');
const port = process.env.PORT || 7000;
const path = require('path');

// routes
const userRoute = require('../route/userAuth');
const adminRoute = require('../route/admin/adminAuth');
const categoryRoute = require('../route/category');
const productRoute = require('../route/product');
const cartRoute = require('../route/cart');




// middleware
app.use(cors());
app.use(express.json());
app.use("/public", express.static(path.join(__dirname, "uploads")));
app.use('/api/v1', userRoute);
app.use('/api/v1', adminRoute);
app.use('/api/v1', categoryRoute);
app.use('/api/v1', productRoute);
app.use('/api/v1', cartRoute);

// Databse
databaseConnect();


app.get('/', (req,res)=>{
    return res.status(200).json({
        message: "Backend server is running successfully!"
    });
});

app.listen(port, ()=>{
    console.log(`server is running on port ${port}`.bold.white);
});