const User = require("../model/userSchema");
const jwt = require("jsonwebtoken");
const { validationResult } = require('express-validator');

exports.signup = (req, res, next) => {
  User.findOne({ email: req.body.email }).exec((error, result) => {
    if (error)
      return res.status(400).json({
        error,
      });

    if (result) {
      return res.status(400).json({
        message: "user already exists",
      });
    }

    const { firstName, lastName, email, password, userName } = req.body;
    const _user = new User({
      firstName,
      lastName,
      email,
      password,
      userName,
    });

    _user.save((error, result) => {
      if (error) return res.status(400).json({ error });
      if (result) {
        res.status(200).json({
          message: "user created successfully!",
        });
      }
    });
  });
};

exports.signin = (req, res, next) => {
  User.findOne({ email: req.body.email }).exec((error, user) => {
    if (error) return res.status(400).json({ error });
    if (user) {
      if (user.authenticate(req.body.password) && user.role === "user") {
        const token = jwt.sign({ _id: user._id, role: user.role }, process.env.JWT_TOKEN, {
          expiresIn: "1d",
        });
        const {
          firstName,
          lastName,
          email,
          password,
          userName,
          _id,
          fullName,
          role,
        } = user;

        return res.status(200).json({
          token,
          user: {
            _id,
            firstName,
            lastName,
            email,
            password,
            userName,
            fullName,
            role,
          },
        });
      } else {
        return res.status(400).json({
          message: "Invalid password",
        });
      }
    } else {
      return res.status(400).json({
        message: "Something is wrong",
      });
    }
  });
};
