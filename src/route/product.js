const express = require('express');
const { requireSignIn, adminMiddleware, userMiddleware } = require('../common-middleware/middleware');
const { addProduct, getProduct } = require('../controller/product');
const multer = require('multer');
const shortid = require('shortid');
const router = express.Router();
const path = require('path');


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(path.dirname(__dirname), 'uploads'))
    },
    filename: function (req, file, cb) {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
      cb(null, shortid.generate() + '-' + file.originalname)
    }
  })
  
  const upload = multer({ storage: storage })


router.post('/product/create', requireSignIn, adminMiddleware, upload.array('productPicture'), addProduct);
router.get('/product/get-product', requireSignIn, userMiddleware, getProduct);

module.exports = router;