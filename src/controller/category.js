const express = require("express");
const { default: slugify } = require("slugify");
const Category = require("../model/category");

function createCategory(category, parentId = null) {
  const categoryList = [];
  let categories;
  if (parentId == null) {
    categories = category.filter((cat) => cat.parentId == undefined);
  } else {
    categories = category.filter((cat) => cat.parentId == parentId);
  }

  for (let cate of categories) {
    categoryList.push({
      _id: cate._id,
      slug: cate.slug,
      name: cate.name,
      children: createCategory(category, cate._id),
    });
  }

  return categoryList;
}

exports.addCategory = (req, res, next) => {
  const categoryObj = {
    name: req.body.name,
    slug: slugify(req.body.name),
  };
  if(req.file){
    categoryObj.categoryImg = process.env.API + '/public/' + req.file.filename;
  }

  if (req.body.parentId) {
    categoryObj.parentId = req.body.parentId;
  }

  const cat = new Category(categoryObj);
  cat.save((errors, category) => {
    if (errors) {
      return res.status(400).json({ errors });
    }
    if (category) {
      return res.status(200).json({ category: category });
    }
  });
};

exports.getCategory = (req, res, next) => {
  Category.find({}).exec((errors, result) => {
    if (errors) {
      return res.status(400).json({ errors });
    }
    if (result) {
      const categoryList = createCategory(result);
      return res.status(200).json({ categoryList });
    }
  });
};
