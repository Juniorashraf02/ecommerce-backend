const express = require('express');
const { requireSignIn, adminMiddleware, userMiddleware } = require('../common-middleware/middleware');
const { addItem } = require('../controller/cart');
const router = express.Router();


router.post('/user/cart/addToCart', requireSignIn, userMiddleware, addItem);

module.exports = router;