const { check, validationResult } = require("express-validator");

exports.validateSignUpReq = [
  check("firstName").notEmpty().withMessage("firstName is required"),
  check("lastName").notEmpty().withMessage("lastName is required"),
  check("email").isEmail().withMessage("valid email is required"),
  check("password")
    .isLength({ min: 6 })
    .withMessage("minimum 6 character password is required"),
    check("userName")
    .notEmpty()
    .withMessage("User name is required")
];
exports.validateSignInReq = [
  check("email").isEmail().withMessage("valid email is required"),
  check("password")
    .isLength({ min: 6 })
    .withMessage("minimum 6 character password is required"),
    check("userName")
    .notEmpty()
    .withMessage("User name is required")
];

exports.isReqValidated = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.array().length > 0) {
    return res.status(400).json({
      message: errors.array()[0].msg,
    });

  }
  next();

};
