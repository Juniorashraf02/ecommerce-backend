const express = require('express');
const { requireSignIn } = require('../../common-middleware/middleware');
const { signup, signin, signout } = require('../../controller/admin/adminAuth');
const { validateSignUpReq, isReqValidated, validateSignInReq } = require('../../validators/auth');
const router = express.Router();

router.post('/admin/signup', validateSignUpReq, isReqValidated, signup);
router.post('/admin/signin',  signin);
router.post('/admin/signout', requireSignIn, signout)

module.exports = router;